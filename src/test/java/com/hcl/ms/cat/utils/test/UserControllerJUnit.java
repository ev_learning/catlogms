package com.hcl.ms.cat.utils.test;

import java.io.IOException;

import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hcl.ms.cat.CatalogueMsApplication;

/**
 * JUnit Helper class for UserControllerTest
 * Initialize MockMvc
 * 
 * @author SushilY
 *
 */

@RunWith(SpringRunner.class)
@SpringBootTest(classes = CatalogueMsApplication.class)
public class UserControllerJUnit {
	protected MockMvc mvc;

	@Autowired
	WebApplicationContext webApplicationContext;

	/**
	 * Initialize MocMvc Object
	 *
	 */
	protected void init() {
		mvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
	}

	/**
	 * @param object Change Object into String
	 *
	 */
	protected String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}

	/**
	 * @param json
	 * @param clas
	 * @return <T>
	 *
	 */

	protected <T> T mapFromJson(String json, Class<T> clas)
			throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.readValue(json, clas);
	}

	/**
	 * Return value from API
	 * 
	 * @param inputJson
	 * @param uri
	 * @return int
	 *
	 */
	public int callApi(String inputJson, String uri) {
		try {
			MvcResult mvcResult = mvc.perform(
					MockMvcRequestBuilders.post(uri).contentType(MediaType.APPLICATION_JSON_VALUE).content(inputJson))
					.andReturn();
			int status = mvcResult.getResponse().getStatus();
			return status;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return 0;
		}
	}
}
