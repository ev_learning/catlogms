package com.hcl.ms.cat.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import com.hcl.ms.cat.model.UserModel;
import com.hcl.ms.cat.service.UserService;
import com.hcl.ms.cat.utils.test.UserControllerJUnit;

/** Create UserControllerTest.class
 * Test UserController.class 
 * @author SushilY
 *
 */
class UserControllerTest extends UserControllerJUnit{
	
	@Mock
	UserService userService;
	
	@BeforeEach
	public void init() {
		super.init();
	}
	@Test
	void testSaveUser() {
		String uri = "/user/add_user";
		UserModel userModel=new UserModel("firstName", "lastName", "M", "test@gmail.com", 78940623145L);
		userModel.setUserId(1);
		Mockito.when(userService.saveUser(userModel)).thenReturn(userModel);
		try {
			String inputJson = mapToJson(userModel);
			int status = callApi(inputJson, uri);
			assertEquals(200, status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
