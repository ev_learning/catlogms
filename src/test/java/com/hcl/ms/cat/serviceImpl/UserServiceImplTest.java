/**
 * 
 */
package com.hcl.ms.cat.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import com.hcl.ms.cat.entity.Catalogue;
import com.hcl.ms.cat.entity.User;
import com.hcl.ms.cat.model.UserModel;
import com.hcl.ms.cat.repository.CatalogueRepository;
import com.hcl.ms.cat.repository.UserRepository;

/**Create Test class for UserServiceImpl
 * @author SushilY
 *
 */
class UserServiceImplTest {
		
	@InjectMocks
	UserServiceImpl userServiceImpl;
	
	@Mock	
	UserRepository userRepository;
	
	@Mock
	CatalogueRepository catalogueRepository; 
	/**
	 * Initialize Mockito
	 */
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
	}
	/**
	 * Test method for {@link com.hcl.ms.cat.serviceImpl.UserServiceImpl#addUser(com.hcl.ms.cat.model.UserModel)}.
	 */
	@Test
	void testAddUser() {
		
		UserModel userModel=new UserModel("umesh", "", "", "",4L);
		User user = new User();		
		Catalogue catalogue=new Catalogue();
		user.set_id(1);
		user.setFirstName("Test");
		catalogue.setCatId(1);
		user.setCatalogue(catalogue);	
		
		//Mockito.when(userModel.getUser()).thenReturn(userModel.getUser());
		
		Mockito.when(userRepository.save(user)).thenReturn(user);
		Mockito.when(catalogueRepository.save(catalogue)).thenReturn(catalogue);
		
		UserModel userModelResponse=userServiceImpl.saveUser(userModel);
		
		assertEquals(user.get_id(), userModelResponse.getUserId());
		
	}

}
