/**
 * 
 */
package com.hcl.ms.cat.serviceImpl;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import com.hcl.ms.cat.entity.Catalogue;
import com.hcl.ms.cat.entity.Product;
import com.hcl.ms.cat.entity.User;
import com.hcl.ms.cat.model.ProductModel;
import com.hcl.ms.cat.repository.ProductRepository;
import com.hcl.ms.cat.repository.UserRepository;

/**Create Test class for ProductServiceImpl
 * @author SushilY
 *
 */
class ProductServiceImplTest {

	@InjectMocks
	ProductServiceImpl pServiceImpl;
	
	@Mock	
	ProductRepository productRepository;
	
	@Mock	
	UserRepository userRepository;
	
	/**
	 * Initialize Mockito
	 */
	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		
	}
	/**
	 * Test method for {@link com.hcl.ms.cat.serviceImpl.ProductServiceImpl#getProductDetails(long)}.
	 */
	@Test
	void testGetProductDetails() {
		Product product=new Product();
		product.setProdId(1);
		product.setName("Test");
		product.setAvailability("H");
		product.setDescription("descript");
		product.setPrice(768.26);
		Catalogue catalogue=new Catalogue();
		catalogue.setCatId(22);
		catalogue.setName("testCatLog");
		product.setCatalogue(catalogue);
		Optional<Product>optional=Optional.of(product);
		
		Mockito.when(productRepository.findById(1L)).
		thenReturn(optional);
		ProductModel pModel = pServiceImpl.findProductDetails(1);
		assertEquals(product.getAvailability(), pModel.getProductAvailability());
	}

	/**
	 * Test method for {@link com.hcl.ms.cat.serviceImpl.ProductServiceImpl#getProductListByUserId(long)}.
	 */
	@Test
	void testGetProductListByUserId() {
		User user=new User();
		user.set_id(1);
		user.setContactNumber(4569825689L);
		user.setEmail("test@gmail.com");
		user.setFirstName("firstName");
		user.setLastName("lastName");
		user.setGender("M");
		Catalogue catalogue=new Catalogue();
		catalogue.setCatId(22);
		catalogue.setName("testCatLog");
		user.setCatalogue(catalogue);
		
		Product product=new Product();
		product.setProdId(1);
		product.setName("Test");
		product.setAvailability("H");
		product.setDescription("descript");
		product.setPrice(778.26);
		product.setCatalogue(catalogue);
		
		Catalogue catalogue1=new Catalogue();
		catalogue1.setCatId(22);
		catalogue1.setName("testCatLog");
		Product product1=new Product();
		product1.setProdId(2);
		product1.setName("Est");
		product1.setAvailability("O");
		product1.setDescription("descript");
		product1.setPrice(798.26);
		product1.setCatalogue(catalogue1);
		
		Catalogue catalogue2=new Catalogue();
		catalogue2.setCatId(24);
		catalogue2.setName("testCatLog");
		Product product2=new Product();
		product2.setProdId(3);
		product2.setName("Test");
		product2.setAvailability("L");
		product2.setDescription("descript");
		product2.setPrice(768.26);
		product2.setCatalogue(catalogue2);
		
		List<Product>pList=new ArrayList<>();
		pList.add(product);
		pList.add(product1);
		pList.add(product2);
		
		Mockito.when(userRepository.findUserById(Mockito.anyLong())).thenReturn(user);
		Mockito.when(productRepository.
				findByCatalogueCatIdOrderByNameAscPriceAsc(Mockito.anyLong())).
		thenReturn(pList);
		List<ProductModel> pModelList = pServiceImpl.findAllProductListByUserId(1);
		assertEquals(3, pModelList.size());
	}

	/**
	 * Test method for {@link com.hcl.ms.cat.serviceImpl.ProductServiceImpl#updateProductDetails(com.hcl.ms.cat.model.ProductModel)}.
	 */
	@Test
	void testUpdateProductDetails() {
		Product product=new Product();
		product.setProdId(1);
		product.setName("Test");
		product.setAvailability("H");
		product.setDescription("descript");
		product.setPrice(768.26);
		Catalogue catalogue=new Catalogue();
		catalogue.setCatId(22);
		catalogue.setName("testCatLog");
		product.setCatalogue(catalogue);
		Optional<Product>optional=Optional.of(product);
		
		Mockito.when(productRepository.findById(1L)).
		thenReturn(optional);
		Mockito.when(productRepository.save(product)).thenReturn(product);
		ProductModel pModel = new ProductModel(1, "Test Value", 222, "TestproductDescription", "H", 21);
		boolean hasUpdated = pServiceImpl.updateProductDetails(pModel);
		assertEquals(true, hasUpdated);
	}

	/**
	 * Test method for {@link com.hcl.ms.cat.serviceImpl.ProductServiceImpl#deleteByProductId(long)}.
	 */
	@Test
	void testDeleteByProductId() {
		Mockito.when(productRepository.findById(1L)).
		thenReturn(Optional.ofNullable(null));
		Mockito.doNothing().when(productRepository).deleteById(1L);		
		boolean hasDeleted=pServiceImpl.deleteByProductId(1L);
		assertEquals(true, hasDeleted);
	}

	/**
	 * Test method for {@link com.hcl.ms.cat.serviceImpl.ProductServiceImpl#findAllProduct(int, int)}.
	 */
	@Test
	void testFindAllProduct() {
		Catalogue catalogue=new Catalogue();
		catalogue.setCatId(22);
		catalogue.setName("testCatLog");
		
		Product product=new Product();
		product.setProdId(1);
		product.setName("Test");
		product.setAvailability("H");
		product.setDescription("descript");
		product.setPrice(778.26);
		product.setCatalogue(catalogue);
		
		Catalogue catalogue1=new Catalogue();
		catalogue1.setCatId(22);
		catalogue1.setName("testCatLog");
		Product product1=new Product();
		product1.setProdId(2);
		product1.setName("Est");
		product1.setAvailability("O");
		product1.setDescription("descript");
		product1.setPrice(798.26);
		product1.setCatalogue(catalogue1);
		
		Catalogue catalogue2=new Catalogue();
		catalogue2.setCatId(24);
		catalogue2.setName("testCatLog");
		Product product2=new Product();
		product2.setProdId(3);
		product2.setName("Test");
		product2.setAvailability("L");
		product2.setDescription("descript");
		product2.setPrice(768.26);
		product2.setCatalogue(catalogue2);
		
		List<Product>pList=new ArrayList<>();
		pList.add(product);
		pList.add(product1);
		pList.add(product2);
		
		Page<Product>pageList=new PageImpl<>(pList);
		
		Pageable pageable=PageRequest.of(2, 3);
		Mockito.when(productRepository.findAll(pageable)).thenReturn(pageList);
		List<ProductModel> productModels=pServiceImpl.findAllProduct(1, 2);
		assertEquals(3, productModels.size());
		
	}

}
