package com.hcl.ms.cat.serviceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.hcl.ms.cat.entity.User;
import com.hcl.ms.cat.model.UserModel;
import com.hcl.ms.cat.repository.CatalogueRepository;
import com.hcl.ms.cat.repository.UserRepository;
import com.hcl.ms.cat.service.UserService;
import com.hcl.ms.cat.utils.UserServiceImplUtils;

/**Create Service class
 * Fetch details from DB
 * Return details to Repository
 * @author SushilY
 *
 */

@Service
@Transactional
public class UserServiceImpl extends UserServiceImplUtils implements UserService {

	@Autowired
	UserRepository userRepository;

	@Autowired
	CatalogueRepository catalogueRepository;

	/**
	 * This function is used to add User info in database...
	 */
	@Override
	public UserModel saveUser(UserModel userModel) {
		// TODO Auto-generated method stub
		User user = getUser(userModel);		
		user=userRepository.save(user);
		user.getCatalogue().setName(user.getFirstName());
		user.setCatalogue(catalogueRepository.save(user.getCatalogue()));
		userModel=user.getUserModel();
		return userModel;
	}
}
