package com.hcl.ms.cat.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.ms.cat.model.NoObjRespnseModel;
import com.hcl.ms.cat.model.PageModel;
import com.hcl.ms.cat.model.ProductModel;
import com.hcl.ms.cat.model.ResponseModel;
import com.hcl.ms.cat.model.UserModel;
import com.hcl.ms.cat.service.ProductService;
import com.hcl.ms.cat.utils.AppConstant;

/** Create Product class
 * Communicate with ProductService interface
 * get And post mapping in respect of Product with server
 * 
 * @author SushilY
 *
 */
@RestController
@RequestMapping(path = "/product")
public class ProductController {

	@Autowired
	ProductService productService;
	
	/**
	 * @param productModel
	 * @return 
	 */
	@PostMapping("/add_product")
	public ResponseEntity<Object> saveProduct(@Valid @RequestBody ProductModel productModel) {
		try {
			ProductModel model = productService.saveProduct(productModel);
			if (model != null) {
				return new ResponseEntity<Object>(new ResponseModel(true, AppConstant.SUCCESS, model), HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_DOES_NOT_EXIST),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(new NoObjRespnseModel(false, e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	/**
	 * @param productModel
	 * @return Object
	 */
	@PostMapping("/find_product_details")
	public ResponseEntity<Object> findProductDetails(@Valid @RequestBody ProductModel productModel) {
		try {
			ProductModel model = productService.findProductDetails(productModel.getProductId());
			if (model != null) {
				return new ResponseEntity<Object>(new ResponseModel(true, AppConstant.SUCCESS, model), HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_DOES_NOT_EXIST),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			return new ResponseEntity<Object>(new NoObjRespnseModel(false, e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * @param userModel
	 * @return List<ProductModel>
	 */
	@PostMapping("/find_all_product_by_user_id")
	public ResponseEntity<Object> findAllProductListByUserId(@Valid @RequestBody UserModel userModel) {
		try {
			List<ProductModel> pList = productService.findAllProductListByUserId(userModel.getUserId());
			if (pList != null && !pList.isEmpty()) {
				if (!pList.isEmpty() && pList.size() > 0) {
					return new ResponseEntity<Object>(new ResponseModel(true, AppConstant.SUCCESS, pList),
							HttpStatus.OK);
				} else {
					return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.CATALOGUE_HAS_NO_PRODUCT),
							HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.USER_DOES_NOT_EXIST),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Object>(new NoObjRespnseModel(false, e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	/**
	 * @param productModel
	 * @return boolean
	 */
	@PostMapping("/update_product_details")
	public ResponseEntity<Object> updateProductDetail(@Valid @RequestBody ProductModel productModel) {
		try {
			boolean hasUpdated = productService.updateProductDetails(productModel);
			if (hasUpdated) {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_UPDATED_SUCCESSFULLY),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_UPDATED_FAILED),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Object>(new NoObjRespnseModel(false, e.getMessage()), HttpStatus.OK);
		}
	}
	/**
	 * @param productModel
	 * @return boolean
	 */
	@PostMapping("/delete_by_product_id")
	public ResponseEntity<Object> deleteProductDetail(@Valid @RequestBody ProductModel productModel) {
		try {
			boolean hasDeleted = productService.deleteByProductId(productModel.getProductId());
			if (hasDeleted) {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_DELETED_SUCCESSFULLY),
						HttpStatus.OK);
			} else {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_DELETED_FAILED),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Object>(new NoObjRespnseModel(false, e.getMessage()), HttpStatus.OK);
		}
	}
	
	/**
	 * @param pageModel
	 * @return
	 */
	@PostMapping("/get_by_pagination")
	public ResponseEntity<Object> getPagination(@Valid @RequestBody PageModel pageModel) {
		try {
			List<ProductModel> pList = productService.findAllProduct(pageModel.getPageNumber(),
					pageModel.getNoOfProducts());
			if (pList != null && !pList.isEmpty()) {
				if (!pList.isEmpty() && pList.size() > 0) {
					return new ResponseEntity<Object>(new ResponseModel(true, AppConstant.SUCCESS, pList),
							HttpStatus.OK);
				} else {
					return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_NOT_AVAILABLE),
							HttpStatus.OK);
				}
			} else {
				return new ResponseEntity<Object>(new NoObjRespnseModel(true, AppConstant.PRODUCT_NOT_AVAILABLE),
						HttpStatus.OK);
			}
		} catch (Exception e) {
			// TODO: handle exception
			return new ResponseEntity<Object>(new NoObjRespnseModel(false, e.getMessage()),
					HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
}
